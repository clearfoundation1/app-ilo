<?php

/**
 * iLO power controller.
 *
 * @category   apps
 * @package    ilo
 * @subpackage controllers
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/docs/developer/apps/ilo
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * iLO power controller.
 *
 * @category   apps
 * @package    ilo
 * @subpackage controllers
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/docs/developer/apps/ilo
 */

class Power extends ClearOS_Controller
{
    /**
     * Display index view.
     *
     * @return view
     */

    function index($server, $id)
    {
        $this->_common('view', $server, $id);
    }

    /**
     * Display edit view.
     *
     * @return view
     */

    function edit($server, $id)
    {
        $this->_common('edit', $server, $id);
    }

    /**
     * Generic view/edit.
     *
     * @param string $mode mode
     *
     * @return view
     */

    function _common($mode, $server, $id)
    {
        // Load libraries
        //---------------

        $this->lang->load('redfish');
        $this->load->library('redfish/Power_Library', $server);

        // Load view data
        //---------------

        try {
            $data['server'] = $server;
            $data['id'] = $id;
            $data['mode'] = $mode;
            $data['info'] = $this->power_library->get_info($id);
            $data['power_options'] = $this->power_library->get_reset_types($id);
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------

        $this->page->view_form('power', $data, lang('redfish_power'));
    }

    /**
     * Power reset.
     *
     * @param string $server
     * @param int $id
     * @param string $reset_type
     *
     * @return redirect
     */

    function reset($server, $id, $reset_type)
    {
        // Load libraries
        //---------------

        $this->lang->load('redfish');
        $this->load->library('redfish/Power_Library', $server);

        try {
            $this->power_library->reset($id, $reset_type);
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }
        
        $this->page->set_status_added();
        redirect('/ilo/system/dashboard/index/'.$server.'/'.$id);
    }
}
