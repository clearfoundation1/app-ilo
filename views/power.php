<?php

/**
 * iLO power view.
 *
 * @category   apps
 * @package    ilo
 * @subpackage views
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/support/documentation/clearos/ilo
 */

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('redfish');

$read_only = TRUE;

///////////////////////////////////////////////////////////////////////////////
// Status
///////////////////////////////////////////////////////////////////////////////

echo form_open('ilo/system/power/index');
echo form_header(lang('redfish_power'));

echo field_input('power_reading', $info['PowerControl'][0]->PowerConsumedWatts, lang('redfish_power_reading'), $read_only);

echo form_footer();
echo form_close();

///////////////////////////////////////////////////////////////////////////////
// Supplies
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////

$reset_types = array();

foreach ($power_options as $key => $value) {
    $reset_types['/app/ilo/system/power/reset/'.$server.'/'.$id.'/'.$value] = $value;
}

$anchors = anchor_multi($reset_types, lang('redfish_power'));

$headers = [
    lang('redfish_bay'),
    lang('redfish_present'),
    lang('base_status'),
    lang('redfish_model'),
    lang('redfish_capacity'),
];
$options['no_action'] = TRUE;

foreach ($info['PowerSupplies'] as $id => $supply) {
    if (preg_match('/absent/i', $supply->Status->State)) {
        $item['details'] = array(
            $id + 1,
            $supply->Status->State,
            '-',
            '-',
            '-'
        );
    } else {
        $capacity = empty($supply->PowerCapacityWatts) ? '' : $supply->PowerCapacityWatts . ' ' . lang('redfish_watts');

        $item['details'] = array(
            $id + 1,
            $supply->Status->State,
            $supply->Status->Health,
            $supply->Model,
            $capacity
        );
    }

    $items[] = $item;
}

echo summary_table(
    lang('redfish_power_supplies'),
    $anchors,
    $headers,
    $items,
    $options
);

