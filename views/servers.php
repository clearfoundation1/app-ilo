<?php

/**
 * iLO targets view.
 *
 * @category   apps
 * @package    ilo
 * @subpackage views
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/support/documentation/clearos/ilo
 */

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('redfish');
$this->lang->load('network');
$this->lang->load('base');

///////////////////////////////////////////////////////////////////////////////
// Headers
///////////////////////////////////////////////////////////////////////////////

$headers = array(
    lang('network_address'),
    lang('base_description'),
);

///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////

if ($mode === 'edit') {
    $anchors = array();
} else {
    $anchors = array();
    $anchors = array(anchor_add('/app/ilo/add'));
}

///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////

foreach ($servers as $basename => $details) {
    $buttons = array(
        anchor_view('/app/ilo/system/dashboard/index/' . $basename . '/1'),
        anchor_edit('/app/ilo/edit/' . $basename . '/1'),
        anchor_delete('/app/ilo/delete/' . $basename . '/1'),
    );
    /*
        anchor_edit('/app/ilo/edit/' . $details['address'], 'low'),
        anchor_delete('/app/ilo/delete/' . $details['address'], 'low')
    */

    $item['title'] = $details['address'];
    $item['action'] = '/app/ilo/edit/' . $datails['address'];
    $item['anchors'] = button_set($buttons);

    $item['details'] = array(
        $details['address'],
        $details['description'],
    );

    $items[] = $item;
}

sort($items);

///////////////////////////////////////////////////////////////////////////////
// Summary table
///////////////////////////////////////////////////////////////////////////////

$options['default_rows'] = 25;

echo summary_table(
    lang('redfish_servers'),
    $anchors,
    $headers,
    $items,
    $options
);
