<?php

/**
 * iLO Ethernet Interfaces view.
 *
 * @category   apps
 * @package    ilo
 * @subpackage views
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/support/documentation/clearos/ilo
 */



///////////////////////////////////////////////////////////////////////////////
// Ethernet Interfaces
///////////////////////////////////////////////////////////////////////////////

$anchors = [];
$headers = [
    lang('redfish_ethernet_interface_id'),
    lang('redfish_ethernet_interface_mac_address'),
    lang('redfish_ethernet_interface_full_duplex'),
    lang('redfish_ethernet_interface_speed'),
    /*lang('redfish_ethernet_interface_host_name'),*/
    lang('base_status'),
];
$options['no_action'] = TRUE;

foreach ($info['list'] as $id => $ethernet_interface) {
   
    $status = $ethernet_interface->Status->State;

    /*if($status != "Enabled")
        continue;*/

    if($ethernet_interface->Status->State == 'Enabled')
        $status = $status .'/'.$ethernet_interface->Status->Health; 
    else
        $status = "Absent";

    $item['details'] = array(
        $ethernet_interface->Id,
        $ethernet_interface->MACAddress,
        $ethernet_interface->FullDuplex,
        $ethernet_interface->SpeedMbps,
        /*$ethernet_interface->Name,*/
        $status
    );
    
    $items[] = $item;
}

echo summary_table(
    lang('redfish_ethernet_interfaces'),
    $anchors,
    $headers,
    $items,
    $options
);