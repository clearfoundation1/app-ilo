<?php

/**
 * iLO Chassis controller.
 *
 * @category   apps
 * @package    ilo
 * @subpackage controllers
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/docs/developer/apps/ilo
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * iLO chassis controller.
 *
 * @category   apps
 * @package    ilo
 * @subpackage controllers
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/docs/developer/apps/ilo
 */

class Chassis extends ClearOS_Controller
{
    /**
     * Display index view.
     *
     * @return view
     */

    function index($server, $id)
    {
        $this->_common('view', $server, $id);
    }

    /**
     * Display edit view.
     *
     * @return view
     */

    function edit($server, $id)
    {
        $this->_common('edit', $server, $id);
    }

    /**
     * Generic view/edit.
     *
     * @param string $mode mode
     *
     * @return view
     */

    function _common($mode, $server, $id)
    {
        // Load libraries
        //---------------

        $this->lang->load('redfish');
        $this->load->library('redfish/Chassis_Library', $server);

        // Load view data
        //---------------

        try {
            $data['mode'] = $mode;
            $data['info'] = $this->chassis_library->get_info($id);
            
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------

        $this->page->view_form('chassis', $data, lang('redfish_power'));
    }
}
