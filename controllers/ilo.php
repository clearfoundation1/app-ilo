<?php

/**
 * iLO controller.
 *
 * @category   apps
 * @package    ilo
 * @subpackage controllers
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/docs/developer/apps/ilo
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * iLO controller.
 *
 * @category   apps
 * @package    ilo
 * @subpackage controllers
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/docs/developer/apps/ilo
 */

class ILO extends ClearOS_Controller
{
    /**
     * iLO summary view.
     *
     * @return view
     */

    function index()
    {
        // Load libraries
        //---------------

        $this->lang->load('ilo');

        // Load views
        //-----------

        $controllers = array('ilo/servers');

        $this->page->view_controllers($controllers, lang('ilo_app_name'));
    }
    function edit($profile, $id)
    {
        // Load libraries
        //---------------

        $this->lang->load('redfish');
        $this->load->library('redfish/Redfish');

        try {
            $data['server'] = $this->redfish->get_settings($profile);
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        if ($_POST) {
            
            try {
                $this->redfish->update_profile($profile, $this->input->post('address'), $this->input->post('description'), $this->input->post('username'), $this->input->post('password'));
                redirect('/ilo');
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }

        $data['profile'] = $profile;
        $data['id'] = $id;

        $this->page->view_form('ilo/server_edit', $data, lang('redfish_servers'));
    }
    function add()
    {
        // Load libraries
        //---------------

        $this->lang->load('redfish');
        $this->load->library('redfish/Redfish');

        if ($_POST) {
            
            try {
                $this->redfish->add_profile($this->input->post('profile'), $this->input->post('address'), $this->input->post('description'), $this->input->post('username'), $this->input->post('password'));
                redirect('/ilo');
            } catch (Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }

        $data = array();

        $this->page->view_form('ilo/server_add', $data, lang('redfish_servers'));
    }

    /**
     * Delete view.
     *
     * @param string $profile profile
     * @param string $id id
     *
     * @return view
     */
    function delete($profile, $id)
    {
        $confirm_uri = '/app/ilo/destroy/' . $profile .'/'. $id;
        $cancel_uri = '/app/ilo';
        $items = array($profile);
        $this->page->view_confirm_delete($confirm_uri, $cancel_uri, $items);
    }

    /**
     * Destroys profile.
     *
     * @param string $profile profile
     *
     * @return view
     */
    function destroy($profile)
    {
        // Load libraries
        //---------------
        $this->lang->load('redfish');
        $this->load->library('redfish/Redfish');
        // Handle delete
        //--------------
        try {
            $this->redfish->delete_profile($profile);
            $this->page->set_status_deleted();
            redirect('/ilo');
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }
    }
}
